#!/bin/bash
#
# @@script: docker-purge-containers.sh
# @@description: docker remove all containers
# @@version: 0.0.1
# @@author: Loouis Low
# @@copyright:
#

function runas_root() {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[docker] Permission denied."
         exit 1
   fi
}

function purge_containers() {
    docker rm $(sudo docker ps -a -q)
}

### init
runas_root
purge_containers
