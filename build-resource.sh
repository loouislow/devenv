#!/bin/bash
#
# @@script: build-resource.sh
# @@description: create resource boilerplate
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

parentdir="NEW.design.title"
subdir_preview="preview"
subdir_psd="psd"
subdir_ai="ai"
subdir_pdf="pdf"
subdir_cmyk="cmyk"
subdir_rgb="rgb"

function build_artwork_structure() {
    echo "[build-resource] building artwork project boilerplate..."
    mkdir -p $parentdir/{$subdir_preview,$subdir_psd,$subdir_ai,$subdir_pdf}
    mkdir -p $parentdir/$subdir_psd/$subdir_cmyk
    mkdir -p $parentdir/$subdir_psd/$subdir_rgb
    mkdir -p $parentdir/$subdir_ai/$subdir_cmyk
    mkdir -p $parentdir/$subdir_ai/$subdir_rgb
}

function build_readme() {
    echo "[build-resource] writing README.md..."
printf "
# Create Artwork Folder

### How To

- rename NEW.design.title to design project title
- parent folder should be a design project title
- sub-folder-preview should containing output artwork (jpg,png)
- sub-folder-psd should containing project artwork (psd,ai)
- sub-folder-pdf should containing output artwork (pdf)

---

### Folder Structure

    NEW.design.title
        ╠═ README.md    (little information of howtos)
        ╠═ preview      (final artwork preview files e.g. .jpg, .png)
        ╠═ psd          (artwork format in Adobe Photoshop e.g. .psd)
        ║   ╠═ cmyk     (indicate PS artwork color profile in CMYK)
        ║   ╚═ rgb      (indicate PS artwork color profike in RGB)
        ╠═ ai           (artwork format in Adobe Illustrator, e.g. .ai)
        ║  ╠═ cmyk      (indicate AI artwork color profile in CMYK)
        ║  ╚═ rgb       (indicate AI artwork color profike in RGB)
        ╚═ pdf          (final artwork output files in Adobe Acrobat e.g. .pdf)

---

### Example

goody.basicplan.material.design > preview

" > $parentdir/README.md

cat $parentdir/README.md
}

### init
build_artwork_structure
build_readme
