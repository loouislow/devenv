#!/bin/bash
#
# @@script: build-project.sh
# @@description: create project boilerplate
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

parentdir="NEW.prj.title"
subdir_1="app.type.prj.title"
subdir_2="html"
subdir_3="front_user_ui"

function build_folder_structure() {
    echo "[build-project] building project boilerplate..."
    mkdir -p $parentdir/$subdir_1/$subdir_2/$subdir_3
}

function build_git_repository() {
    echo "[build-project] creating Git repository..."
    cd $parentdir/$subdir_1/$subdir_2/$subdir_3
    gitinit
}

function build_readme() {
    echo "[build-project] writing README.md..."
printf "
# Create Project Folder

### How To

- rename {goody.prj.title} to a project title
- parent folder should be a project title
- sub-folder-1 should be type of project (ui,web,app)
- sub-folder-2 should be type of technology (html,php,js)
- sub-folder-3 should be type of type of development (front_user_ui, back_user_ui, back_admin_ui, front_user, back_user, back_admin)

---

### Folder Structure

    NEW.prj.title
        ╠═ README.md                (little information of howtos)
        ╚═ app.type.prj.title       (indicate type and project name)
            ╚═ html                 (indicate type of technologies)
                ╚═ front_user_ui    (indicate project category)
                    ╚═ .git         (auto created git repository)

---

### Example

goody.gobiz > app.ui.gobiz > html > front_user_ui
" > README.md

cat README.md
}

function build_gitcogs_watchdog() {
    echo "[build-project] writing gitcogs watchdog script..."
printf "#!/bin/bash
#
# @@script: gitcogs.sh
# @@description: githarvester watchdog
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

function watch_now {
   echo '[gitcogs] Batch git commiting on every 10 secs...'
   githarvest -s 10 $PWD
}

### init
watch_now
" > gitcogs.sh

    chmod +x gitcogs.sh
}

function build_squeezer() {
    echo "[build-project] writing squeezer script..."
printf "#!/bin/bash
#
# @@script: squeezer.sh
# @@description: an image compressor
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

function start_squeezer() {
    squeezer -v -d $PWD
}

### init
start_squeezer
" > squeezer.sh

    chmod +x squeezer.sh
}

### init
build_folder_structure
build_git_repository
build_gitcogs_watchdog
build_squeezer
build_readme
