#!/bin/bash
#
# @@script: setup.sh
# @@description: devenv setup script
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

dest_dir="/usr/local/bin"
app_dir="/usr/src"

function runas_root() {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
     then
       echo -e "\e[34m[devenv-setup]\e[39m permission denied."
     exit 1
   fi
}

function transfer_scripts() {
    echo -e "\e[34m[devenv-setup]\e[39m transfering scripts to" $dest_dir "..."
    cp *.sh ${dest_dir}
    cp templates/backup.conf /etc
}

function copy_apps() {
    echo -e "\e[34m[devenv-setup]\e[39m copying apps to" ${app_dir} "..."
    cp devenv-apps.tar ${app_dir}
    
    echo -e "\e[34m[devenv-setup]\e[39m exracting apps to" ${app_dir} "..."
    tar -xf ${app_dir}/devenv-apps.tar -C ${app_dir}/
    
    echo -e "\e[34m[devenv-setup]\e[39m deleting apps to" ${app_dir} "..."
    rm -rf ${app_dir}/devenv-apps.tar
}

function symlink_scripts() {
    echo -e "\e[34m[devenv-setup]\e[39m symbolic link to" ${dest_dir} "..."
    ln -s /usr/src/devenv-apps/squeezer/squeezer.py ${dest_dir}
    ln -s /usr/src/devenv-apps/liveup/liveup.js ${dest_dir}
    ln -s /usr/src/devenv-apps/echoes/run-stack.sh ${dest_dir}
    ln -s /usr/src/devenv-apps/echoes/clean-stack.sh ${dest_dir}
    
    mv ${dest_dir}/liveup.js ${dest_dir}/liveup
    mv ${dest_dir}/squeezer.py ${dest_dir}/squeezer
    mv ${dest_dir}/run-stack.sh ${dest_dir}/echoes-start
    mv ${dest_dir}/clean-stack.sh ${dest_dir}/echoes-purge
    
    cd ${dest_dir}
    bash ${dest_dir}/rnmsh.sh
    rm -rf ${dest_dir}/rnmsh
}

### init
runas_root
transfer_scripts
copy_apps
symlink_scripts
