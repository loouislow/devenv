#!/bin/bash
#
# @@script: sleepproof.sh
# @@description: check if binary or service is sleeping
# @@author: Loouis Low
# @@copyright: GNU GPL
#

if pgrep -x "gedit" > /dev/null
then
    echo "[nosleep] Running"
else
    echo "[nosleep] Stopped"
    gedit
fi
