#!/bin/bash
#
# @@script: rnmsh.sh
# @@description: rename devenv scripts
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

dest_dir="/usr/local/bin"

function runas_root() {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
     then
       echo -e "\e[34m[devenv-setup]\e[39m permission denied."
     exit 1
   fi
}

function rename_ext() {
    echo -e "\e[34m[devenv-setup]\e[39m change script extension at" ${dest_dir} "..."
    for file in *.sh
    do
        mv "$file" "${file%.sh}"
    done
}

### init
runas_root
rename_ext

