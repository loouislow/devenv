#!/bin/bash
#
# @@script: buildsubproject.sh
# @@description: create sub-project boilerplate
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

subdir_1="SUB.app.type.prj.title"
subdir_2="html"
subdir_3="front_user_ui"

function build_boilerplate() {
    echo "\e[34m[buildproject]\e[39m building project boilerplate..."
    mkdir -p $subdir_1/$subdir_2/$subdir_3
    ls -l
}

function build_git_repo() {
    echo "\e[34m[buildproject]\e[39m building Git repository..."
    cd $subdir_1/$subdir_2
    gitinit
}

function build_readme() {
printf "
# Create Project Folder

### How To

- rename {goody.prj.title} to a project title
- parent folder should be a project title
- sub-folder-1 should be type of project (ui,web,app)
- sub-folder-2 should be type of technology (html,php,js)
- sub-folder-3 should be type of type of development (front_user_ui, back_user_ui, back_admin_ui, front_user, back_user, back_admin)

---

### Folder Structure

    app.type.prj.title          (indicate type and project name)
        ╠═ README.md            (little infomation of howtos)
        ╚═ html                 (indicate type of technologies)
            ╠═ .git             (auto created git repository)
            ╚═ front_user_ui    (indicate project category)

---

### Example

goody.gobiz > app.ui.gobiz > html > front_user_ui

---

" > $subdir_1/README.md

cat $subdir_1/README.md
}

function build_gitcogs() {
printf "#!/bin/bash
#
# @@script: gitcogs.sh
# @@description: Example watchdog settings
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### watchdog
function watch_now {
   echo '[gitcogs] Batch running...'
   githarvest -s 5 $PWD
}

### initialize
watch_now
" > gitcogs.sh

chmod +x gitcogs.sh
}

### init
build_boilerplate
build_git_repo
build_gitcogs
build_readme
