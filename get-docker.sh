#!/bin/bash
#
# @@script: get-docker.sh
# @@description: docker installer
# @@version: 0.0.1
# @@author: Loouis Low
# @@copyright:
#

function runas_root() {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
      then
         echo "[docker] Permission denied."
         exit 1
   fi
}

function get_packages() {
    apt-get -y install \
        apt-transport-https \
        ca-certificates \
        curl \
        python-software-properties
}

function add_repo() {
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian \
        $(lsb_release -cs) \
        stable"
}

function update_repo() {
    apt-get update
}

function install_docker() {
    apt-get -y install docker-ce
}

### init
runas_root
get_packages
add_repo
update_repo
install_docker
